# NUAV Gazebo  
This repo contains NUAV `models` and `worlds` for Gazebos simulations. In the future, it might also contain plugins for custom behaviors.

## Repo Usage
To add the worlds and models to gazebo, run the following commands 
(preferably is a setup.sh or .bashrc)
```bash
source /usr/share/gazebo/setup.sh
export GAZEBO_MODEL_PATH=${GAZEBO_MODEL_PATH}:nuav_gazebo/nuav_gazebo/models
export GAZEBO_RESOURCE_PATH=${GAZEBO_RESOURCE_PATH}:nuav_gazebo/nuav_gazebo/worlds
```


## Tips
Convert URDF to SDF ([post](https://answers.gazebosim.org//question/2282/convert-urdf-to-sdf-or-load-urdf/)):  
```bash
gz sdf -p /my_urdf.urdf > /my_sdf.sdf
```  
Calculating motor parameters: https://github.com/engcang/mavros-gazebo-application/tree/master/px4_model_making#-motor
### Add new motor config
Look in `PX4-Autopilot/ROMFS/px4fmu_common/init.d/airframes/` to see if a configuration already exists. Also, check `PX4-Autopilot/ROMFS/px4fmu_common/init.d-posix/airframes/` and confirm there's a config file there also.  
If there isn't a config file in `init.d-posix` copy the relevant one from `init.d` and add the file to `PX4-Autopilot/ROMFS/px4fmu_common/init.d-posix/airframes/CMakeLists.txt`.  
For new configurations a config file must be created in `init.d` and `init.d-posix`. A mixer file must also be added to `PX4-Autopilot/ROMFS/px4fmu_common/mixers`. A relevant frame configuration should also be added to `PX4-Autopilot/src/lib/mixer/MultirotorMixer/geometries`. That should be all.
